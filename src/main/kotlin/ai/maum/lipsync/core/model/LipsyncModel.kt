package ai.maum.lipsync.core.model

import kotlinx.serialization.Serializable

@Serializable
data class LipsyncModel(
        val host: String,
        val port: Int,
        val open: Boolean? = false
)