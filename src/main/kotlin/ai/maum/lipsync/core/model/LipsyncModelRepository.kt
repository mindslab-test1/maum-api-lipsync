package ai.maum.lipsync.core.model

interface LipsyncModelRepository {
    fun getModelInfo(clientId: String, model: String): LipsyncModel
    fun update(lipsyncModelUpdate: LipsyncModelUpdate)
    fun testGetModels(): MutableMap<String, Pair<LipsyncModel, MutableSet<String>>>
}