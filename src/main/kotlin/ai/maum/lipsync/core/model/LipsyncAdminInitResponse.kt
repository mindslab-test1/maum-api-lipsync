package ai.maum.lipsync.core.model

import kotlinx.serialization.Serializable

@Serializable
data class LipsyncAdminInitResponse(
        val status: Int? = 0,
        val message: String? = "",
        val payload: LipsyncInitData = LipsyncInitData()
)

@Serializable
data class LipsyncInitData(
        val modelList: List<LipsyncModelWithUsers> = mutableListOf()
)

@Serializable
data class LipsyncModelWithUsers(
        val model: LipsyncModel,
        val modelName: String,
        val users: Set<String>
)