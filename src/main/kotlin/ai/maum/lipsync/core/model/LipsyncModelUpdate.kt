package ai.maum.lipsync.core.model

import kotlinx.serialization.Serializable

@Serializable
data class LipsyncModelUpdate(
        val action: String? = "none",
        val modelName: String? = "",
        val modelInfo: LipsyncModel? = null,
        val clientAction: String? = "none",
        val client: String? = null
)