package ai.maum.lipsync.core.usecase

import ai.maum.lipsync.boundaries.dto.LipsyncStatusDto
import ai.maum.lipsync.boundaries.dto.LipsyncUploadDto
import ai.maum.lipsync.core.model.LipsyncModel
import ai.maum.lipsync.core.model.LipsyncModelRepository
import ai.maum.lipsync.core.service.LipsyncService
import ai.maum.lipsync.util.ResponseFormat
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import java.io.ByteArrayOutputStream
import java.io.OutputStream

@Component
class LipsyncUsecase(
        val lipsyncModelRepository: LipsyncModelRepository,
        val lipsyncService: LipsyncService
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val keyInfoMap = mutableMapOf<String, RequestKeyInfo>()

    fun upload(clientId: String, dto: LipsyncUploadDto): Any {
        val lipsyncModel = lipsyncModelRepository.getModelInfo(clientId, dto.model
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "model"))
        val requestKey = lipsyncService.upload(lipsyncModel, dto)

        if (dto.image == null && dto.transparent == true)
            keyInfoMap[requestKey] = RequestKeyInfo(clientId, ResponseFormat.WEBM.value, dto.model)
        else keyInfoMap[requestKey] = RequestKeyInfo(clientId, ResponseFormat.MP4.value, dto.model)

        return requestKey
    }

    fun getStatus(clientId: String, dto: LipsyncStatusDto): Any{
        val requestKey = dto.requestKey ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "request key not provided")
        val requestKeyInfo = keyInfoMap[requestKey] ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid request key")
        if(requestKeyInfo.clientId != clientId) throw ResponseStatusException(HttpStatus.FORBIDDEN, "requested resource is not allowed for $clientId")
        val lipsyncModel = lipsyncModelRepository.getModelInfo(clientId, requestKeyInfo.modelName)

        return lipsyncService.getStatus(lipsyncModel, requestKeyInfo.modelName, requestKey)
    }

    fun download(clientId: String, requestKey: String): Pair<OutputStream, String>{
        val requestKeyInfo = keyInfoMap[requestKey] ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid request key")
        if(requestKeyInfo.clientId != clientId) throw ResponseStatusException(HttpStatus.FORBIDDEN, "requested resource is not allowed for $clientId")
        val lipsyncModel = lipsyncModelRepository.getModelInfo(clientId, requestKeyInfo.modelName)

        return Pair(
                lipsyncService.download(lipsyncModel, requestKeyInfo.modelName, requestKey),
                "attachment; filename=maum_ai_lipsync_$requestKey.${requestKeyInfo.format}"
        )
    }

    data class RequestKeyInfo(
            val clientId: String,
            val format: String,
            val modelName: String
    )
}