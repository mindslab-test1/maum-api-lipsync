package ai.maum.lipsync.core.service

import ai.maum.lipsync.boundaries.dto.LipsyncStatus
import ai.maum.lipsync.boundaries.dto.LipsyncUploadDto
import ai.maum.lipsync.core.model.LipsyncModel
import java.io.OutputStream

interface LipsyncService {
    fun upload(lipsyncModel: LipsyncModel, lipsyncUploadDto: LipsyncUploadDto): String
    fun getStatus(lipsyncModel: LipsyncModel, modelName: String, requestKey: String): LipsyncStatus
    fun download(lipsyncModel: LipsyncModel, modelName: String, requestKey: String): OutputStream
}