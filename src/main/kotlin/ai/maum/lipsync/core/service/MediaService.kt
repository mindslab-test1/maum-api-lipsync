package ai.maum.lipsync.core.service

import org.springframework.web.multipart.MultipartFile

interface MediaService {
    fun saveContent(file: MultipartFile)
}