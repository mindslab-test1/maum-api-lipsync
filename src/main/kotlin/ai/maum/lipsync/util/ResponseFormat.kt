package ai.maum.lipsync.util

enum class ResponseFormat(val value: String) {
    MP4("mp4"),
    WEBM("webm")
}