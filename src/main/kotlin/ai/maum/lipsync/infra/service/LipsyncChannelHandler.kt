package ai.maum.lipsync.infra.service

import io.grpc.ManagedChannel

interface LipsyncChannelHandler {
    fun getOrPutChannel(modelName: String, host: String, port: Int): ManagedChannel
    fun removeChannel(modelName: String?): Unit
}