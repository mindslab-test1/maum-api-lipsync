package ai.maum.lipsync.infra.service

import ai.maum.lipsync.boundaries.dto.LipsyncStatus
import ai.maum.lipsync.boundaries.dto.LipsyncUploadDto
import ai.maum.lipsync.core.exception.ApiInternalException
import ai.maum.lipsync.core.model.LipsyncModel
import ai.maum.lipsync.core.service.LipsyncService
import com.google.protobuf.ByteString
import io.grpc.stub.StreamObserver
import maum.brain.lipsync.BrainLipsync
import maum.brain.lipsync.LipSyncGenerationGrpc
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.lang.Exception
import java.lang.RuntimeException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@Service
class LipsyncServiceImpl(
        val lipsyncChannelHandler: LipsyncChannelHandler
): LipsyncService {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun upload(lipsyncModel: LipsyncModel, lipsyncUploadDto: LipsyncUploadDto): String {
        val channel = lipsyncChannelHandler.getOrPutChannel(
                lipsyncUploadDto.model!!,
                lipsyncModel.host,
                lipsyncModel.port
        )
        val stub = LipSyncGenerationGrpc.newStub(channel)

        val finishLatch = CountDownLatch(1)
        val resultList = mutableListOf<BrainLipsync.LipSyncRequestKey>()
        val errorList = mutableListOf<Throwable>()

        val requestObserver = stub.upload(object: StreamObserver<BrainLipsync.LipSyncRequestKey> {
            override fun onNext(value: BrainLipsync.LipSyncRequestKey) {
                resultList.add(value)
                logger.debug("Request Key received: ${value.requestKey}")
            }

            override fun onError(t: Throwable?) {
                logger.error("Err from responseObserver during upload", t)
                errorList.add(t?: Exception("unknown"))
                finishLatch.countDown()
            }

            override fun onCompleted() {
                logger.debug("Finished from responseObserver upload")
                finishLatch.countDown()
            }
        })

        val inputBuilder = BrainLipsync.LipSyncInput.newBuilder()
        requestObserver.onNext(inputBuilder
                .setText(lipsyncUploadDto.text)
                .setResolution(this.getResolutionEnum(lipsyncUploadDto.resolution!!))
                .setTransparent(lipsyncUploadDto.transparent!! && lipsyncUploadDto.image == null)
                .build())

        lipsyncUploadDto.image?.let { imageFile ->
            val byteArrayInputStream = ByteArrayInputStream(imageFile.bytes)

            var len: Int
            val buffer = ByteArray(1024 * 512)
            while (byteArrayInputStream.read(buffer).also { len = it } > 0){
                if(buffer.size > len) {
                    val inputByteString = ByteString.copyFrom(buffer.sliceArray(IntRange(0, len - 1)))
                    requestObserver.onNext(inputBuilder.setBackground(inputByteString).build())
                }
                else{
                    val inputByteString = ByteString.copyFrom(buffer)
                    requestObserver.onNext(inputBuilder.setBackground(inputByteString).build())
                }
            }

            byteArrayInputStream.close()
        }

        requestObserver.onCompleted()

        try {
            if(!finishLatch.await(1, TimeUnit.MINUTES))
                throw ApiInternalException(message = "connection timeout")
        } catch (e: InterruptedException) {
            logger.error(e.localizedMessage)
            throw RuntimeException(e)
        }

        if(errorList.isNotEmpty()){
            logger.error(errorList[0].message)
            throw ApiInternalException(message = "internal error has occurred")
        }
        if(resultList.isEmpty()){
            throw ApiInternalException(message = "internal error has occurred")
        }

        return resultList[0].requestKey
    }

    override fun getStatus(lipsyncModel: LipsyncModel, modelName: String, requestKey: String): LipsyncStatus {
        val channel = lipsyncChannelHandler.getOrPutChannel(
                modelName,
                lipsyncModel.host,
                lipsyncModel.port
        )

        val stub = LipSyncGenerationGrpc.newBlockingStub(channel)
        val statusMessage = stub.statusCheck(BrainLipsync.LipSyncRequestKey.newBuilder()
                .setRequestKey(requestKey)
                .build())

        logger.debug(statusMessage.statusCodeValue.toString())
        logger.debug(statusMessage.msg)
        logger.debug(statusMessage.waiting.toString())

        return LipsyncStatus(
                statusCode = statusMessage.statusCodeValue,
                message = statusMessage.msg,
                waiting = statusMessage.waiting
        )
    }

    override fun download(lipsyncModel: LipsyncModel, modelName: String, requestKey: String): OutputStream {
        val channel = lipsyncChannelHandler.getOrPutChannel(
                modelName,
                lipsyncModel.host,
                lipsyncModel.port
        )

        val stub = LipSyncGenerationGrpc.newBlockingStub(channel)
        val requestKeyMessage = BrainLipsync.LipSyncRequestKey.newBuilder()
                .setRequestKey(requestKey)
                .build()
        val statusMessage = stub.statusCheck(requestKeyMessage)

        if(statusMessage.statusCodeValue != 2){
            logger.warn(statusMessage.statusCode.toString())
            logger.warn(statusMessage.msg)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "process is not finished")
        }

        val rawIterator = stub.download(requestKeyMessage)

        val outputStream = ByteArrayOutputStream()

        try {
            while (rawIterator.hasNext()){
                val response = rawIterator.next()
                outputStream.write(response.video.toByteArray())
            }
        } catch (e: Exception){
            logger.error("Error: Stream response from download avatar")
            logger.error(e.message, e)
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)
        }

        return outputStream
    }

    private fun getResolutionEnum(resolution: String): BrainLipsync.VideoResolution {
        return when(resolution.toUpperCase()){
            "HD" -> BrainLipsync.VideoResolution.HD
            "SD" -> BrainLipsync.VideoResolution.SD
            else -> throw ResponseStatusException(HttpStatus.BAD_REQUEST, "unsupported resolution")
        }
    }
}