package ai.maum.lipsync.infra.service

import ai.maum.lipsync.core.exception.*
import ai.maum.lipsync.core.model.LipsyncModel
import ai.maum.lipsync.core.model.LipsyncModelRepository
import ai.maum.lipsync.core.model.LipsyncModelUpdate
import ai.maum.lipsync.core.model.LipsyncAdminInitResponse
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.server.ResponseStatusException

@Service
class LipsyncModelRepositoryImpl(
        adminWebClient: WebClient,
        private val lipsyncChannelHandler: LipsyncChannelHandler
): LipsyncModelRepository {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val memoryMap = mutableMapOf<String, Pair<LipsyncModel, MutableSet<String>>>()

    init {
        val initRequest = adminWebClient
                .get()
                .uri("/admin/api/init/getInitialData")
                .header("ai-maum-engine", "LIPSYNC")
                .retrieve()

        val responseRaw = initRequest.bodyToMono(String::class.java)
                .block() ?: throw ApiTransactionalException(
                message = "response from admin is null!"
        )
        logger.debug("${Json.decodeFromString<LipsyncAdminInitResponse>(responseRaw)}")

        val responsePayload = Json.decodeFromString<LipsyncAdminInitResponse>(responseRaw).payload
        responsePayload.modelList.forEach { modelWithUsers ->
            val modelName = modelWithUsers.modelName
            val modelInfo = LipsyncModel(
                    host = modelWithUsers.model.host,
                    port = modelWithUsers.model.port,
                    open = modelWithUsers.model.open,
            )
            val clientSet = modelWithUsers.users as MutableSet<String>
            memoryMap[modelName] = Pair(first = modelInfo, second = clientSet)
        }

        logger.info("loaded model list from admin server")
        for(mutableEntry in memoryMap) {
            logger.debug(mutableEntry.key)
            logger.debug(mutableEntry.value.toString())
        }
    }

    override fun getModelInfo(clientId: String, model: String): LipsyncModel {
        val modelPair = memoryMap[model] ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "requested model is not present")
        if(!(modelPair.first.open == true || modelPair.second.contains(clientId)))
            throw  ResponseStatusException(HttpStatus.FORBIDDEN, "model not authenticated for client")

        return modelPair.first
    }

    override fun update(lipsyncModelUpdate: LipsyncModelUpdate) {
        when(lipsyncModelUpdate.action){
            "create" -> {
                createAction(lipsyncModelUpdate)
            }
            "update" -> {
                updateAction(lipsyncModelUpdate)
            }
            "delete" -> {
                val removed = memoryMap.remove(lipsyncModelUpdate.modelName)
                logger.info("removed model: ${lipsyncModelUpdate.modelName}")
                logger.debug("model info: $removed")
                lipsyncChannelHandler.removeChannel(lipsyncModelUpdate.modelName)
            }
            "none" -> {
                logger.warn("no action message presented")
            }
            else -> throw MqUnknownActionException(
                    message = "unknown action ${lipsyncModelUpdate.action}",
                    mqObject = lipsyncModelUpdate
            )
        }
    }

    override fun testGetModels(): MutableMap<String, Pair<LipsyncModel, MutableSet<String>>> {
        return this.memoryMap
    }

    private fun createAction(lipsyncModelUpdate: LipsyncModelUpdate){
        lipsyncModelUpdate.modelInfo ?: throw MqInsufficientDataException(
                message = "model info not specified in message",
                mqObject = lipsyncModelUpdate
        )
        lipsyncModelUpdate.client ?: throw MqInsufficientDataException(
                message = "model creator(default user) not specified in message",
                mqObject = lipsyncModelUpdate
        )

        val pair = Pair(
                first = lipsyncModelUpdate.modelInfo,
                second = mutableSetOf(lipsyncModelUpdate.client)
        )
        memoryMap[lipsyncModelUpdate.modelName ?: throw MqConCurrencyException(
                message = "null model name presented"
        )] = pair
        logger.info("created model: ${lipsyncModelUpdate.modelName}")
        logger.debug("model info: ${lipsyncModelUpdate.modelInfo}")

    }

    private fun updateAction(lipsyncModelUpdate: LipsyncModelUpdate){
        memoryMap[lipsyncModelUpdate.modelName] ?: throw MqConCurrencyException(
                message = "concurrency exception: no known model name '${lipsyncModelUpdate.modelName}', " +
                        "known models: ${memoryMap.keys}"
        )

        val clientSet: MutableSet<String> = LinkedHashSet()
        clientSet.addAll(memoryMap[lipsyncModelUpdate.modelName]?.second ?: throw ApiInternalException(
                message = "could not find client set for model: ${lipsyncModelUpdate.modelName}"
        ))
        val originModel = memoryMap[lipsyncModelUpdate.modelName]?.first ?: throw ApiInternalException(
                message = "could not find origin model info: ${lipsyncModelUpdate.modelName}"
        )

        when (lipsyncModelUpdate.clientAction){
            "add" -> clientSet.add(lipsyncModelUpdate.client ?: throw MqInsufficientDataException(
                    message = "client info not specified in message"
            ))
            "remove" -> clientSet.remove(lipsyncModelUpdate.client ?: throw MqInsufficientDataException(
                    message = "client info not specified in message"
            ))
        }

        memoryMap[lipsyncModelUpdate.modelName ?: throw MqConCurrencyException(
                message = "concurrency exception: no known model name '${lipsyncModelUpdate.modelName}', " +
                        "known models: ${memoryMap.keys}"
        )] = Pair(
                first = lipsyncModelUpdate.modelInfo ?: originModel,
                second = clientSet
        )

        logger.info("updated model: ${lipsyncModelUpdate.modelName}")
        logger.debug("model info: ${lipsyncModelUpdate.modelInfo}")
        logger.debug("client action: ${lipsyncModelUpdate.clientAction}, client: ${lipsyncModelUpdate.client}")

        lipsyncChannelHandler.removeChannel(lipsyncModelUpdate.modelName)
    }
}