package ai.maum.lipsync

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LipsyncApplication

fun main(args: Array<String>) {
    runApplication<LipsyncApplication>(*args)
}
