package ai.maum.lipsync.boundaries.controller

import ai.maum.lipsync.boundaries.dto.LipsyncResponseDto
import ai.maum.lipsync.boundaries.dto.LipsyncStatusDto
import ai.maum.lipsync.boundaries.dto.LipsyncUploadDto
import ai.maum.lipsync.core.usecase.LipsyncUsecase
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.io.ByteArrayOutputStream
import javax.validation.Valid

@RestController
@RequestMapping("/lipsync")
class LipsyncController (
        val lipsyncUsecase: LipsyncUsecase,
        private val jwtDecoder: JwtDecoder
){
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @PostMapping("upload", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun lipsyncUpload(
            @Valid uploadDto: LipsyncUploadDto,
            oAuth2Authentication: OAuth2Authentication
    ): Any {
        val clientId = this.extractClientId(oAuth2Authentication)
        val responsePayload = lipsyncUsecase.upload(clientId, uploadDto)
        val responseBody = LipsyncResponseDto(payload = responsePayload)
        return ResponseEntity.ok(responseBody)
    }

    @PostMapping("statusCheck", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun lipsyncStatus(
            @RequestBody statusDto: LipsyncStatusDto,
            oAuth2Authentication: OAuth2Authentication
    ): Any {
        val clientId = this.extractClientId(oAuth2Authentication)
        val responsePayload = lipsyncUsecase.getStatus(clientId, statusDto)
        val responseBody = LipsyncResponseDto(payload = responsePayload)
        return ResponseEntity.ok(responseBody)
    }

    @GetMapping("download", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    fun lipsyncDownload(
            @RequestParam("requestKey") requestKey: String,
            oAuth2Authentication: OAuth2Authentication
    ): Any {
        val clientId = this.extractClientId(oAuth2Authentication)
        val responseData = lipsyncUsecase.download(clientId, requestKey)
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, responseData.second)
                .body((responseData.first as? ByteArrayOutputStream
                        ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)).toByteArray())
    }


    private fun extractClientId(oAuth2Authentication: OAuth2Authentication): String{
        val tokenValue = (oAuth2Authentication.details as OAuth2AuthenticationDetails).tokenValue
        val jwtMap = jwtDecoder.decode(tokenValue)
        return jwtMap.getClaim("app_id")
    }
}