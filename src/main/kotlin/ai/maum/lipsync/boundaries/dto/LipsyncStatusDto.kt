package ai.maum.lipsync.boundaries.dto

data class LipsyncStatusDto(
        val requestKey: String?
)