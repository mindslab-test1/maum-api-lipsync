package ai.maum.lipsync.boundaries.dto

import org.springframework.web.multipart.MultipartFile

data class LipsyncUploadDto(
        val text: String?,
        var image: MultipartFile?,
        val model: String? = "baseline",
        val transparent: Boolean? = false,
        val resolution: String? = "HD"
)