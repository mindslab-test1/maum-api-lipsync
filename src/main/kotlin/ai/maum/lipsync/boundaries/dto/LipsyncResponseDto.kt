package ai.maum.lipsync.boundaries.dto

data class LipsyncResponseDto(
        val status: Int? = 0,
        val message: String? = "Success",
        val payload: Any? = null
)

data class LipsyncStatus(
        val statusCode: Int,
        val message: String,
        val waiting: Int
)